// Silence some warnings so they don't distract from the exercise.
#![allow(unused_variables)]

use functions::greet;
use rand::{thread_rng, RngCore, Rng};
fn main() {
    do_stuff();
    println!("{}", do_stuff2(2.0, 2.5));
    println!("{}", do_stuff3(2.0, 3.0));

    println!();
    exercise_b();
    println!();

    // modules
    functions::greet();
    greet();
    let x = thread_rng().gen_range(0, 100);
    println!("{}", x);
}

fn do_stuff() {
    println!("doing stuff");
}

fn do_stuff2(qty: f64, oz: f64) -> f64 {
    return qty * oz;
}

// same as above but without 'return' and semicolon
fn do_stuff3(qty: f64, oz: f64) -> f64 {
    qty * oz
}

// Exercise B
fn exercise_b() {
    println!("==== Exercise B ====");
    let width = 4;
    let height = 7;
    let depth = 10;
    // 1. Try running this code with `cargo run` and take a look at the error.
    //
    // See if you can fix the error. It is right around here, somewhere.  If you succeed, then
    // doing `cargo run` should succeed and print something out.
    //{
        let area = area_of(width, height);
    //}
    println!("Area is {}", area);

    // 2. The area that was calculated is not correct! Go fix the area_of() function below, then run
    //    the code again and make sure it worked (you should get an area of 28).

    // 3. Uncomment the line below.  It doesn't work yet because the `volume` function doesn't exist.
    //    Create the `volume` function!  It should:
    //    - Take three arguments of type i32
    //    - Multiply the three arguments together
    //    - Return the result (which should be 280 when you run the program).
    //
    // If you get stuck, remember that this is *very* similar to what `area_of` does.
    //
    println!("Volume is {}", volume(width, height, depth));
}

fn volume(x: i32, y: i32, z: i32) -> i32 {
    x * y * z
}

fn area_of(x: i32, y: i32) -> i32 {
    // 2a. Fix this function to correctly compute the area of a rectangle given
    // dimensions x and y by multiplying x and y and returning the result.
    //
    //return 0;
    x * y
    // Challenge: The previous line is not idiomatic (not recommended best practice).
    //            Run `cargo clippy`, figure out what's wrong, and fix it.  Once it is fixed,
    //            `cargo clippy` won't return areas, and `cargo run` will still produce the same
    //            output. See also https://github.com/rust-lang/rust-clippy
}

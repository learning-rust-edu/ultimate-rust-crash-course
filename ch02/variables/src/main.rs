fn variables_initialisation() {
    let bunnies = 5;
    println!("{}", bunnies);
    let bunnies1: i32 = 6;
    println!("{}", bunnies1);

    // initialising multiple variables
    let (bunnies2, carrots) = (8, 50);
    println!("{}, {}", bunnies2, carrots);

    // by default variables are immutable, next line will not compile
    // bunnies = 2;
    // if value of variable should be changed, then this variable must be made mutable
    let mut bunnies3 = 32;
    println!("{}", bunnies3);
    // now reassigning value will work
    bunnies3 = 2;
    println!("{}", bunnies3);

    // constants
    const WARP_FACTOR: f64 = 9.9;
    println!("{}", WARP_FACTOR);
}

fn variable_scope() {
    let x = 5;
    {
        let y = 6;
        println!("{}, {}", x, y);
    }
    // next line will not compile because y is out of scope
    // println!("{}, {}", x, y);

    // variable shadowing
    let z = 5;
    {
        let z = 99;
        println!("{}", z);  // prints 99
    }
    println!("{}", z);      // prints 5

    let w = 5;  // w is mutable
    let w = w;  // w is now immutable
    println!("{}", w);
}

fn memory_safety() {
    let mut enigma: i32;
    // next line will not compile because enigma is not initialised
    // println!("enigma is {}", enigma);
    if true {
        enigma = 42;
    }
    // still will not compile because enigma might not be initialised
    // println!("enigma is {}", enigma);
    if true {
        enigma = 42;
    } else {
        enigma = 7;
    }
    // not this will compile
    println!("enigma is {}", enigma);
}

const STARTING_MISSILES: i32 = 8;
const READY_AMOUNT: i32 = 2;

fn exercise_a() {
    println!(" ==== Exercise A ====");
    // Part 1
    let missiles = 8;
    let ready = 2;
    println!("Firing {} of my {} missiles...", ready, missiles);
    println!();

    // Part 2
    let mut missiles1: i32 = STARTING_MISSILES;
    let ready1: i32 = READY_AMOUNT;
    println!("Firing {} of my {} missiles...", ready1, missiles1);
    missiles1 = missiles1 - ready1;
    println!("{} missiles left", missiles1);
    println!();

    // extra
    let (mut missiles2, ready2) = (STARTING_MISSILES, READY_AMOUNT);
    println!("Firing {} of my {} missiles...", ready2, missiles2);
    println!("{} missiles left", (missiles2 - ready2));
}

fn main() {
    variables_initialisation();
    println!();
    variable_scope();
    println!();
    memory_safety();
    println!();
    exercise_a();
}
